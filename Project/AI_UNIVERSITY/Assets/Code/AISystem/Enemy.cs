﻿using System;
using Core;
using Core.Utility;
using UnityEngine;

namespace AISystem
{
	public class Enemy : CachedMonoBehaviour, IDamageable
	{
		public static event Action BossDeath;
		public static event Action<Enemy> EnemyDeath;
		
		[SerializeField] private float _maxHP = 100f;
		[SerializeField] private bool _isBoss = false;
		[SerializeField] private SpriteRenderer _spriteRenderer = default;
		[SerializeField] private float _onHitLerpEffectDuration = 0.25f;
        [SerializeField] private Vector3[] stateScale;
        [SerializeField] private GameObject shine;
        [SerializeField] private Animator endAnimator;
        private Vector3 originalScale, currentScale;
        

		private Collider2D _collider;
		private AudioSource _audio;
		[SerializeField] private Animator _animator;
		private EnemyBrain _brain;
		private float _currentHP;
		private Color _originalColor;
		private Color _hitColor;
		private bool _gotHit;
		private float _hitLerpDuration;
		private bool _isDead;
		public bool IsInvincible
		{
			get;
			set;
		}

		public float NormalizedHealth => _currentHP / _maxHP;
		public Animator Animator => _animator;
		public bool IsPlayer => false;
		public bool IsDead => _currentHP <= 0;
		
		protected override void Awake()
		{
			base.Awake();

            ServiceLocator.gameEnd = false;
			_animator = GetComponentInChildren<Animator>();
			_audio = GetComponentInChildren<AudioSource>();
			_collider = GetComponentInChildren<Collider2D>();
			_originalColor = _spriteRenderer.color;
			_hitColor = new Color(_originalColor.r, _originalColor.g, _originalColor.b, 0.25f);
			_currentHP = _maxHP;
            originalScale = transform.localScale;
            currentScale = originalScale;

            ServiceLocator.currentBoss = this;
        }

		private void Update()
		{
           // if (Input.GetKeyDown("f")) OnHit(100);
            transform.localScale = Vector3.Lerp(transform.localScale, currentScale, 0.2f * Time.deltaTime * 50);
			// Debug only TODO remove
			if (Input.GetKeyDown(KeyCode.K))
			{
				Die();
			}

			if (_gotHit)
			{
				_spriteRenderer.color = Color.Lerp(_hitColor, _originalColor, _hitLerpDuration / _onHitLerpEffectDuration);
				_hitLerpDuration += Time.deltaTime;
				if (_spriteRenderer.color.Equals(_originalColor))
				{
					_gotHit = false;
				}
			}
		}

        [SerializeField, Header("HP Statechange threshholds")] private Vector2 stateThreshholds;    //Exactly 3 threshholds are needed, with the third one being always zero
        private int currentPhase = 3;   //default is the number of phases
		public bool OnHit(int damage)
		{
			if (IsInvincible)
			{
				return false;
			}
			
			_currentHP -= damage;
			_spriteRenderer.color = _hitColor;
			_hitLerpDuration = 0f;
			_gotHit = true;


            _animator.SetInteger("HP", (int)_currentHP);
            ChangePhases();

            return true;
		}

		private void Die()
		{
            _collider.enabled = false;
            _animator.SetTrigger("Die");
            shine.SetActive(true);
            endAnimator.SetTrigger("AfterDeath");
            

            //if (_isBoss)
            //{
            //	BossDeath?.Invoke();
            //}
            //else
            //{
            //	_collider.enabled = false;
            //	_animator.SetTrigger("Die");
            //	EnemyDeath?.Invoke(this);
            //	Destroy(gameObject, 0.5f);
            //}
        }       

        public void PlayAudio(AudioClip clip, bool loop)
		{
			_audio.clip = clip;
			_audio.loop = loop;
			_audio.Play();
		}

		public void StopAudio()
		{
			_audio.Stop();
		}

		public void Kill()
		{
			OnHit((int) _maxHP);
		}

        private void ChangePhases()
        {            
            //each phase has a different threshhold
            switch (currentPhase)
            {
                case 3:
                    if (_currentHP < stateThreshholds.x)
                    {
                        ChangeToNewState(false);
                        currentScale = stateScale[0];
                    }
                    break;

                case 2:
                    if (_currentHP < stateThreshholds.y)
                    {
                        currentScale = stateScale[1];
                        ChangeToNewState(false);
                    }
                    break;

                case 1:
                    if (_currentHP < 0)
                    {
                        ChangeToNewState(true);
                        Die();
                    }
                    break;

                case 0:
                    break;
            }          
        }

        private void ChangeToNewState(bool last)
        {
            if(last) ServiceLocator.effects.FlashScreen(2);
            else ServiceLocator.effects.FlashScreen(1);
            _animator.SetTrigger("ChangePhase");
            currentPhase--;
        }
	}
}