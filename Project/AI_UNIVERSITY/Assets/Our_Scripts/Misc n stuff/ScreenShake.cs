using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShake : MonoBehaviour
{
    private static Vector3 originalPos;

    private void Start()
    {
        originalPos = transform.localPosition;
        ServiceLocator.shake = this;
    }

    public void ExecuteShake(float duration, float magnitude)
    {
        StartCoroutine(Shake(duration, magnitude));
    }

    public IEnumerator Shake(float duration, float magnitude)
    {


        float elapsed = 0.0f;

            while (elapsed < duration)
            {
                float x = Random.Range(-3f, 3f) * (magnitude);

                float y = Random.Range(-3f, 3) * (magnitude);

                transform.localPosition += new Vector3(x, y, 0);

                elapsed += Time.deltaTime;

                yield return null;

            }

        transform.localPosition = originalPos;

    }
    }
