using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobbyP3 : MonoBehaviour
{
    [SerializeField] private DamageOnCollision dmg;
    [SerializeField] private AudioSource source;
    [SerializeField] private AudioClip bang;

    public void EnableDMG()
    {
        dmg.enabled = true;
        StartCoroutine(ServiceLocator.shake.Shake(0.05f, 0.3f));
        source.PlayAudio(bang);
    }

    public void DisableDMG()
    {
        dmg.enabled = false;
    }
}
