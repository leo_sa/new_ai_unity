using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AISystem.Critters.TerrorToothFloater;
using Core.Utility;

public class MoveBehaiviour : StateMachineBehaviour //Coroutinen sind ein nogo :(
{
    private Animator anim;
    private float timerThreshhold = 1;
    private float internTimer;

    public int internCounter;
    
    private bool facingRight = true;
    private bool isGrounded = true;
    private bool punch = false;
    private int moveDirectionX;
    private float resetCoolDown;

    [SerializeField] private float coolDown;
    [SerializeField] private float originalGravity;
    [SerializeField] float moveSpeed;
    [SerializeField] float jumpHeight;
    [SerializeField] LayerMask groundLayer;
    [SerializeField] private AudioClip[] jumpSounds;
    [SerializeField] private AudioClip landSound;
    [SerializeField] private bool useProceduralSprites = true;
    int maxCounter;

    private Globby enemy;
    private bool groundedFlag;
    private ScreenShake shake;


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        maxCounter = Random.Range(2, 5);
        anim = animator;
        enemy = FindObjectOfType<Globby>();
        moveDirectionX = enemy.lastVelocitySign;
        resetCoolDown = coolDown;
        shake = FindObjectOfType<ScreenShake>();
        enemy.proceduralSprite = useProceduralSprites;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        isGrounded = Physics2D.OverlapBox(enemy.groundCheck.position, enemy.boxSize, 0, groundLayer);

        if(groundedFlag != isGrounded)
        {
            groundedFlag = OnGroundedX(isGrounded);
        }

        if (enemy.transform.position.x < -8)
        {
            moveDirectionX = 1;
        }
        else if (enemy.transform.position.x > 3)
        {
            moveDirectionX = -1;
        }

        if (isGrounded)
        {
            coolDown -= Time.deltaTime;
            if (coolDown <= 0 && internCounter < 3 && !punch)
            {
                enemy.enemyRigidbody.velocity = new Vector2(moveDirectionX * moveSpeed * 10, enemy.enemyRigidbody.velocity.y);
                enemy.enemyRigidbody.gravityScale = 0;
                if (Utilities.Chance(70))
                {
                    enemy.enemyRigidbody.velocity += Vector2.up * jumpHeight;
                }
                else
                {
                    enemy.enemyRigidbody.velocity += Vector2.up * jumpHeight * 0.8f;
                }
                enemy.enemyRigidbody.gravityScale = originalGravity;
                internCounter++;
                enemy.ChangeScale(new Vector3(0.5f, 1.5f));
                PlayJumpAudio();

                coolDown = resetCoolDown;
            }

            
            if (internCounter >= 3)
            {            
                punch = true;
                internCounter = 0;
            }
        }
        if(useProceduralSprites) ChangeSprite(enemy.enemyRigidbody.velocity.y);
        CheckWhereToFace();

    }

    
    private bool OnGroundedX(bool dir)
    {
        if (dir)
        {
            OnGroundedEnter();
        }
        else
        {
            OnGroundedExit();
        }
        return dir;
    }

    void OnGroundedEnter()
    {
        enemy.OnGroundedSprite(downSprite, defaultSprite);
        PlayLandAudio();
        //jumpheight/25 because handsome boi should have more intense ScreenShake
        if(punch) ChangeAnimatorState();
        shake.ExecuteShake(0.05f * (jumpHeight/25), 0.02f);
        enemy.ChangeScale(new Vector3(1.5f, 0.5f));
    }

    void OnGroundedExit()
    {
        
    }

    void PlayJumpAudio()
    {
        int index = Random.Range(0, jumpSounds.Length);
        ServiceLocator.audioManager.PlayAudio(jumpSounds[index]);
    }

    void PlayLandAudio()
    {
        if(landSound != null) ServiceLocator.audioManager.PlayAudio(landSound);
    }

    private void CheckWhereToFace()
    {
        if (moveDirectionX > 0)
        {
            facingRight = false;
        }
        else if (moveDirectionX < 0)
        {
            facingRight = true;
        }

        if (((facingRight) && (enemy.localScale.x < 0)) || ((!facingRight) && (enemy.localScale.x > 0)))
        {
            enemy.localScale.x *= -1;
        }

        enemy.transform.localScale = enemy.localScale;
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy.proceduralSprite = false;
        enemy.lastVelocitySign = moveDirectionX;
    }

    private bool changeFlag;
    private bool changeDef;
    private float lastInput;
    [SerializeField] private Sprite upSprite, downSprite, defaultSprite;
    private void ChangeSprite(float input)
    {
        if(input > 4)
        {
            enemy.sprite = upSprite;
            changeFlag = false;
            changeDef = true;
        }
        else if(input < -8)
        {
            if (changeDef)
            {
                enemy.sprite = defaultSprite;
                changeDef = false;
            }
            //enemy.sprite = downSprite;
            changeFlag = false;

            Debug.Log("stuff");
        }
        else
        {
            if (!changeFlag)
            {
                //enemy.sprite = defaultSprite;
                changeFlag = true;
            }
        }

        lastInput = input;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}

    public void ChangeAnimatorState()
    {
        maxCounter = Random.Range(2, 5);
        anim.SetTrigger("punch");
        punch = false;
    }
}
