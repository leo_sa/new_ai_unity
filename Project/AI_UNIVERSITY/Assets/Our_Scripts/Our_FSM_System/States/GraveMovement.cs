using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraveMovement : StateMachineBehaviour
{
    [SerializeField] private float maxSpeed = 0.1f;
    [SerializeField, Header("Max Number of times Player can pass")] private int maxPassPlayer = 3;
    [SerializeField, Header("Pos.x-X and pos.x+Y")] private Vector2 moveThreshold;
    private int moveDir;        //either -1 or 1
    private int lastDistanceSign = -1;   //last sign(-1/1) of playerDistance.
    private int passCounter;
    private Globby glob;
    private Transform playerTransform;
    private float speedMul = 1;

    //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        passCounter = maxPassPlayer;
        glob = FindObjectOfType<Globby>();      //taking the instance from a form of manager would probably be better suited
        glob.proceduralSprite = true;
        moveThreshold = new Vector2(glob.originalPos.x + moveThreshold.x, glob.originalPos.x + moveThreshold.y);
        playerTransform = ServiceLocator.playerTransform;

        Vector2 distance = playerTransform.position - glob.transform.position;
        moveDir = -(int)Mathf.Sign(distance.x);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        glob.transform.Translate((Vector2.right * moveDir * maxSpeed * Time.deltaTime * 50) * speedMul);
        if (glob.transform.position.x < -8)
        {
            speedMul = 5;
            moveDir *= -1;
        }
        else if (glob.transform.position.x > 5)
        {
            speedMul = 5;
            moveDir *= -1;
        }
        glob.sprite = CurrentSprite(moveDir);
        speedMul = Mathf.Lerp(speedMul, 1, 0.05f * Time.deltaTime * 50);
        AttackCheck(animator);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        glob.proceduralSprite = false;
    }

    [SerializeField] private Sprite left, right;
    private Sprite CurrentSprite(int input)
    {
        Sprite returnSprite = input > 0 ? right : left;
        return returnSprite;
    }

    //if passing the boss, attack will be initiated
    private void AttackCheck(Animator anim)
    {
        Vector2 distance = playerTransform.position - glob.transform.position;
        if(Mathf.Sign(distance.x) != lastDistanceSign)
        {
            if(passCounter <= 0)
            {
                ChangeAnimatorState(anim);
                passCounter = Random.Range(2, maxPassPlayer);
            }
            else
            {
                passCounter--;
            }         
            lastDistanceSign = (int)Mathf.Sign(distance.x);
        }
        Debug.Log(passCounter);
    }

    public void ChangeAnimatorState(Animator anim)
    {
        anim.SetTrigger("punch");
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
