using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    [SerializeField]
    private string sceneName;

    [SerializeField]
    private Animator animator;

    [SerializeField]
    private SpriteRenderer fadeOut;

    [SerializeField]
    private int speed;

    [SerializeField]
    private Color transparent, nonTransparent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            StartCoroutine(SceneChange());
        }
    }

    IEnumerator SceneChange()
    {
        animator.SetTrigger("CircleFade");

        yield return new WaitForSecondsRealtime(1.8f);

        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }
}
