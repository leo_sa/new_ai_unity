using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Globby : MonoBehaviour
{
    public int scaleDir = 1;
    [SerializeField] private BoxCollider2D boxCol;
    public Rigidbody2D enemyRigidbody;
    public Transform groundCheck;
    public Vector2 boxSize;
    public Vector3 localScale;
    public Vector2 originalPos;
    public int lastVelocitySign;
    private int scaleDirInt;
    public SpriteRenderer spriteRen;
    public Sprite sprite;
    public bool proceduralSprite;


    //animation related

    [SerializeField] private Transform toAnimate;
    private Vector3 originalScale, targetScale;

    // Use this for initialization
    void Start()
    {
        localScale = transform.localScale;
        enemyRigidbody = GetComponent<Rigidbody2D>();
        originalPos = transform.position;
        originalScale = toAnimate.localScale;
    }

    private void Update()
    {
        spriteRen.sprite = null;
        toAnimate.localScale = Vector3.Lerp(toAnimate.localScale, originalScale, 0.1f * Time.deltaTime * 50);
    }

    private void LateUpdate()
    {
        if (proceduralSprite) spriteRen.sprite = sprite;
    }

    public void ChangeScale(Vector3 targetMultiplicator)
    {
        Debug.Log("ChangedScale");
        toAnimate.localScale = new Vector3(originalScale.x * targetMultiplicator.x, originalScale.y * targetMultiplicator.y, 1);
    }

    public void ChangeScaleDir(int dir)
    {
        scaleDirInt = (int)Mathf.Sign(dir);
    }

    public void TurnIntangiable()
    {
        FindObjectOfType<DamageOnCollision>().enabled = false;
        GetComponent<Rigidbody2D>().gravityScale = 0;
        boxCol.enabled = false;
    }

    public void OnGroundedSprite(Sprite target, Sprite original)
    {
        StartCoroutine(GroundSprite(target, original));
    }

    IEnumerator GroundSprite(Sprite target, Sprite original)
    {
        sprite = target;
        yield return new WaitForSeconds(0.2f);
        sprite = original;
    }
}
