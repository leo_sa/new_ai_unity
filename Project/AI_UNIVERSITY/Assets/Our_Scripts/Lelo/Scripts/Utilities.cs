using UnityEngine;

public static class Utilities
{
    // A simple Chance for if statements
    public static bool Chance(int chance)
    {
        int n = Random.Range(0, 100);

        if (n <= chance) return true;
        else return false;
    }


    public static void PlayAudio(this AudioSource source, AudioClip clip)
    {
        source.Stop();
        source.clip = clip;
        source.pitch = Random.Range(0.95f, 1.05f);
        source.Play();
    }
}
