using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;

//Used for our slime boss
public class DamageOnCollision : MonoBehaviour
{
    [SerializeField] private CircleCollider2D collider2;
    protected virtual bool OwnerIsPlayer => false;
    [SerializeField] private int _damage = 1;
    [SerializeField] private SpriteRenderer spriteRenderer;
    private Transform playerPos;

    private void Start()
    {
        playerPos = ServiceLocator.playerTransform;
    }


    public void DecideDir()
    {
        float difference = playerPos.position.x - transform.position.x;
        difference = Mathf.Sign(difference);

        bool flipX = difference > 0 ? true : false;
        spriteRenderer.flipX = flipX;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        IDamageable damageable = other.GetComponentInParent<IDamageable>();
        if ((damageable != null) && (damageable.IsPlayer != OwnerIsPlayer))
        {
            bool dealtDamage = damageable.OnHit(_damage);
        }
    }

    private void OnEnable()
    {
        ChangeCoolider(true);
    }

    private void OnDisable()
    {
        ChangeCoolider(false);        
    }

    private void ChangeCoolider(bool dir)
    {
        collider2.enabled = dir;
    }
}
