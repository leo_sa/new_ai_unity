Shader "Unlit/SpriteLocalized"
{
    Properties
    {
        [NoScaleOffset]_MainTex ("Texture", 2D) = "white" {}
		[NoScaleOffset]_DamageLUT ("Texture", 2D) = "white" {}
		[HDR]_Color("color", color) = (0, 0, 0, 0)
	}
		SubShader
		{
			Tags { 
			"Queue" = "Transparent"
			"RenderType" = "Opaque"
				}

			Pass
			{
			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha
				HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _DamageLUT;
            float4 _Color;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

			void FinalColor(float2 uv, float4 input, out float4 Out) {
				float4 lut = tex2D(_DamageLUT, uv);
				Out = lerp(input, _Color, lut);
			}

            float4 frag (v2f i) : SV_Target
            {
				float4 col = tex2D(_MainTex, i.uv);
				FinalColor(i.uv, col, col);

                return col;
            }
            ENDHLSL
        }
    }
}
