using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EffectMaster : MonoBehaviour
{
    [SerializeField] private Image img;
    private Color col, col_H;
    private float lengthMultiplier;
    private float currentTimescale = 1;
    [SerializeField] private AudioSource source;
    [SerializeField] private AudioClip a, b;

    private void Start()
    {
        ServiceLocator.effects = this;
        col = new Color(1, 1, 1, 0);
        col_H = new Color(1, 1, 1, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        if(img.color != col)
        {
            img.color = Color.Lerp(img.color, col, 0.1f * Time.deltaTime * 50 / lengthMultiplier);
        }

        if(currentTimescale != 1)
        {
            currentTimescale = Mathf.Lerp(currentTimescale, 1, 0.1f * Time.deltaTime * 50 / lengthMultiplier);
        }
        SetTime(currentTimescale);
    }


    public void FlashScreen(float lengthMul)
    {
        StartCoroutine(ServiceLocator.shake.Shake(0.05f * lengthMultiplier, 0.05f * lengthMultiplier));
        img.color = col_H;
        lengthMultiplier = lengthMul;
        currentTimescale = 0.5f;

        AudioClip cachedClip = lengthMul == 1 ? a : b;
        source.PlayAudio(cachedClip);
    }

    public void Slomo()
    {
        currentTimescale = 0.5f;
    }

    private void SetTime(float value)
    {
        Time.timeScale = value;
        Time.fixedDeltaTime = value * 0.02f;
    }
}
