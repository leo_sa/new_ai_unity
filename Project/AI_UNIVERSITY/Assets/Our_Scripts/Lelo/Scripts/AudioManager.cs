using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioSource source;
    [SerializeField] private AudioClip[] attackA, attackB;
    [SerializeField] private AudioClip transformation_B, transformation_C, death;

    private void Start()
    {
        ServiceLocator.audioManager = this;
        Debug.Log(gameObject);
    }

    public void PlayAttackA()
    {
        AudioClip cachedClip = attackA[Random.Range(0, attackA.Length)];
        source.PlayAudio(cachedClip);
    }

    public void PlayAttackB()
    {
        AudioClip cachedClip = attackB[Random.Range(0, attackB.Length)];
        source.PlayAudio(cachedClip);
    }

    public void PlayTransformB()
    {
        source.PlayAudio(transformation_B);
    }

    public void PlayTransformC()
    {
        source.PlayAudio(transformation_C);
    }

    public void PlayDeath()
    {
        source.PlayAudio(death);
    }

    public void PlayAudio(AudioClip clip)
    {
        source.PlayAudio(clip);
    }
}
