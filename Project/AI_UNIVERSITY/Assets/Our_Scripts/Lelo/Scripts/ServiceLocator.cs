using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AISystem;

public static class ServiceLocator
{
    public static Enemy currentBoss;    //In our case, only one boss will be active at a time so this variable is set directly from Enemy.cs   
    public static Transform playerTransform; //currently set from CharacterController.cs
    public static AudioManager audioManager;
    public static ScreenShake shake;
    public static bool isAttacking;
    public static bool gameEnd;
    public static EffectMaster effects;
}
